FROM python:latest

WORKDIR /app/
ADD requirements.txt /app/
RUN python3 -m pip install -r requirements.txt
