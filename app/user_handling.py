from app import login_manager
from app.models import User


@login_manager.user_loader
def load_user(user_id):
    return User.objects.get(user_id=user_id)
