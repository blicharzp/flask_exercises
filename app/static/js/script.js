const NAV_BAR = function () {
    var keys = ['index', 'courses', 'register', 'login', 'enrollment', 'chat', 'logout'];
    var values = document.querySelectorAll(`.nav-link`);
    var result = {};

    keys.forEach((key, i) => result[key] = values[i]);
    return result
}();

function getCurrentPageName() {
    return document.querySelector("meta[name=template]").content.slice(0, -5);
}

function main() {
    var currentPageName = getCurrentPageName();
    NAV_BAR[currentPageName].classList.add("active");
}

main();
