const socket = io();
socket.on('connect', function () {
    socket.emit('join', `${user_email}`);
});

socket.on('join', function (message) {
    var conteiner = document.querySelector(".chat-container");
    conteiner.append(message);
});

socket.on('message', function (msg) {
    console.log(msg);
});

socket.on('disconnect', function () {
    console.log('disconnected');
});