import os
import secrets


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY', secrets.token_urlsafe())
    MONGODB_SETTINGS = {
        'db': os.environ['DB_NAME'],
        'host': os.environ['DB_SERVICE_NAME'],
        'port': int(os.environ['DB_SERVICE_PORT']),
    }
