from app import socketio

from flask_socketio import send, emit

# @socketio.on('message')
# def handle_message(message):
#     send(f"DUPA {message}")


@socketio.on('join')
def handle_message(user):
    emit("join", f"{user} joined.")
