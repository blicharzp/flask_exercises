from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, StringField
from wtforms.validators import DataRequired, Email, Length, EqualTo, ValidationError
from app.models import Enrollment, User


class LoginForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[
                             DataRequired(), Length(min=6, max=15)])
    remember_me = BooleanField("Remember Me")


class RegisterForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[
                             DataRequired(), Length(min=6, max=15)])
    password_confirm = PasswordField("Confirm Password", validators=[EqualTo('password'),
                                                                     DataRequired(), Length(min=6, max=15)])
    first_name = StringField("First Name", validators=[
                             DataRequired(), Length(min=2, max=20)])
    last_name = StringField("Last Name", validators=[
                            DataRequired(), Length(min=2, max=20)])

    def validate_email(self, email):
        if User.objects(email=email.data).first():
            raise ValidationError(f"{email.data} is already taken.")
