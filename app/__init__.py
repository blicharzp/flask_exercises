from flask import Flask
from flask_mongoengine import MongoEngine
from flask_socketio import SocketIO
from flask_login import LoginManager
import os

db = MongoEngine()
socketio = SocketIO()
login_manager = LoginManager()


def create_app():
    app = Flask(__name__)
    from app.config import Config

    app.config.from_object(Config)

    db.init_app(app)
    socketio.init_app(app)
    login_manager.init_app(app)

    from app.models.populate import populate
    if bool(os.environ.get('POPULATE_DB')):
        populate()

    from app import user_handling

    from app import routes
    from app import api

    app.register_blueprint(routes.bp)
    app.register_blueprint(api.bp, url_prefix='/api')

    return app


if __name__ == '__main__':
    socketio.run(create_app())
