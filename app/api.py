from flask import Blueprint, request, jsonify
from app.models import Course, User
from werkzeug.security import generate_password_hash

bp = Blueprint('api', __name__)


@bp.route("/users/<int:user_id>")
def user(user_id):
    return jsonify(User.objects.get_or_404(user_id=user_id))


@bp.route("/users/", methods=["GET", "POST", "DELETE"])
def users():
    if request.method == "POST":
        user_id = User.objects().order_by("-user_id").limit(-1).first().user_id + 1
        try:
            User(user_id=user_id, first_name=request.json.get('first_name'), last_name=request.json.get('last_name'),
                 email=request.json.get('email'), password=generate_password_hash(request.json.get('password'))).save()
        except:
            return jsonify(User.objects.get(email=request.json.get('email'))), 418
        return jsonify(User.objects.get_or_404(user_id=user_id))
    if request.method == "DELETE":
        user = User(user_id=request.json.get('user_id'))
        user.delete()
        return jsonify(user)
    return jsonify(User.objects)


@bp.route("/courses/<string:course_id>")
def course(course_id):
    return jsonify((Course.objects.get_or_404(course_id=course_id)))


@bp.route("/courses/")
def courses():
    return jsonify(Course.objects)
