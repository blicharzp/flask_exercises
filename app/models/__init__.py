from app import db
from werkzeug.security import check_password_hash
from flask_login import UserMixin


class User(db.Document, UserMixin):
    user_id = db.IntField(primary_key=True)
    first_name = db.StringField(max_length=50)
    last_name = db.StringField(max_length=50)
    email = db.StringField(max_length=50, unique=True)
    password = db.StringField()

    @staticmethod
    def match(email, password):
        user = User.objects.get(email=email)
        if user and check_password_hash(user.password, password):
            return user
        return None


class Course(db.Document):
    course_id = db.StringField(max_length=10, primary_key=True)
    title = db.StringField(max_length=100)
    description = db.StringField(max_length=255)
    credits = db.IntField()
    term = db.StringField(max_length=25)


class Enrollment(db.Document):
    user_id = db.ReferenceField(User)
    course_id = db.ReferenceField(Course)
