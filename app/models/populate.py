from . import Course, User
import mongoengine
import json
from werkzeug.security import generate_password_hash

DB_DATA_PATH = "static/models/"


def populate():
    populate_courses()
    populate_users()


def populate_courses():
    with open(f"{DB_DATA_PATH}/courses.json") as fp:
        for row in json.load(fp):
            try:
                Course(course_id=row['courseID'], title=row['title'], description=row['description'],
                       credits=row['credits'], term=row['term']).save()
            except mongoengine.errors.NotUniqueError:
                pass


def populate_users():
    with open(f"{DB_DATA_PATH}/users.json") as fp:
        for row in json.load(fp):
            try:
                User(user_id=row['id'], first_name=row['first_name'], last_name=row['last_name'],
                     email=row['email'], password=generate_password_hash(row['password'])).save()
            except mongoengine.errors.NotUniqueError:
                pass
