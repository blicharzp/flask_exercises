from flask import Blueprint, render_template, request, flash, url_for, redirect, session
from app.forms import LoginForm, RegisterForm
from app.models import Course, User, Enrollment
from werkzeug.security import generate_password_hash
from flask_login import login_user, login_required, logout_user, current_user


bp = Blueprint('routes', __name__)


@bp.route("/")
def index():
    return render_template("index.html")


@bp.route("/courses/", defaults={"term": "Spring 2019"})
@bp.route("/courses/<term>")
def courses(term):
    return render_template("courses.html", courses=Course.objects, term=term)


@bp.route("/register", methods=["GET", "POST"])
def register():
    if session.get('user_id'):
        flash(f"You are already logged in.", "success")
        return redirect(url_for('routes.index'))
    form = RegisterForm()
    if form.is_submitted():
        if form.validate():
            return handle_valid_user(form)
        else:
            return handle_form_error(form, 'routes.register')
    return render_template("register.html", form=form)


def handle_valid_user(form):
    user_id = User.objects().order_by("-user_id").limit(-1).first().user_id + 1
    User(user_id=user_id, first_name=form.first_name.data, last_name=form.last_name.data,
         email=form.email.data, password=generate_password_hash(form.password.data)).save()
    flash(
        f"You have successfully registered {form.first_name.data} user.", "success")
    return redirect(url_for('routes.index'))


@bp.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        flash(f"You are already logged in.", "success")
        return redirect(url_for('routes.index'))
    form = LoginForm()
    if form.is_submitted():
        if form.validate():
            return handle_user_match(User.match(form.email.data, form.password.data))
        else:
            return handle_form_error(form, 'routes.login')
    return render_template("login.html", form=form)


def handle_user_match(user):
    if user:
        flash(f"Hello {user.first_name}.", "success")
        login_user(user)
    else:
        flash("Email does not match password.", "danger")
    return redirect(url_for('routes.index'))


def handle_form_error(form, route):
    for field, error in form.errors.items():
        flash(f"{field}: {error[0]}", "danger")
    return redirect(url_for(route))


@bp.route("/logout")
@login_required
def logout():
    logout_user()
    flash(f"You are succesfully logged out.", "success")
    return redirect(url_for('routes.index'))


@bp.route("/enrollment", methods=["GET", "POST"])
@login_required
def enrollment():
    if request.method == "POST":
        return handle_enrollment_post()
    return handle_enrollment_get()


def handle_enrollment_post():
    course_id = request.form.get('course_id')
    user_id = current_user.user_id
    if course_id and user_id:
        if Enrollment.objects(course_id=course_id, user_id=user_id):
            flash(
                f"You are already enrolled for course {course_id}", "warning")
            return redirect(url_for('routes.courses'))
        else:
            Enrollment(course_id=course_id, user_id=user_id).save()
            flash(f"You have succesfully signed in for {course_id}", "success")
            return redirect(url_for('routes.enrollment'))


def handle_enrollment_get():
    user_id = current_user.user_id
    courses = Enrollment.objects.filter(
        user_id=user_id).order_by('course_id').values_list('course_id')
    user = User.objects.get(user_id=user_id)
    return render_template("enrollment.html", first_name=user.first_name, last_name=user.last_name, courses=courses)


@bp.route("/chat")
@login_required
def chat():
    return render_template("chat.html")
