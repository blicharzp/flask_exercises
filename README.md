# Flask framework studies

## Requirements 
`docker` 
`docker-compose` 

## How to run

### Docker Compose
To build image:
```
docker-compose build
```
To run services:
```
docker-compose up
```
## Database 
### Database population
Set proper enviroment variable:
```
export POPULATE_DB=1
```
### Database wipe
Down docker-compose volumes:
```
docker-compose down -v
```

